/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.minecraft.server;

/**
 *
 * @author Smile
 */
import java.util.Random;

public class BlockWhiteStoneM extends Block
{
    public BlockWhiteStoneM(int i)
    {
        super(i, Material.STONE);
        this.a(CreativeModeTab.b);
    }

    /**
     * Returns the ID of the items to drop on destruction.
     */
    public int getDropType(int i, Random random, int j)
    {
    	return ((random.nextInt(25) == 5) ? Block.blockWhiteStoneTiny.id : Block.blockWhiteStoneM.id); 
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
   
}
