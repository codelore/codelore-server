/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.minecraft.server;

/**
 *
 * @author Smile
 */
import java.util.Random;

public class BlockReshetka extends Block
{
    private boolean localFlag;

    public BlockReshetka(int i)
    {
        super(i, Material.WOOD);
        this.a(CreativeModeTab.c);
    }

    public int idDropped(int i, int j)
    {
        return Block.blockReshetka.id;
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int a(Random random)
    {
        return 1;
    }

    /**
     * Is this block (a) opaque and (b) a full 1m cube?  This determines whether or not to render the shared face of two
     * adjacent blocks and also whether the player can attach torches, redstone wire, etc to this block.
     */
    public boolean isOpaqueCube() {
		return false;
	}

    /**
     * Returns Returns true if the given side of this block type should be rendered (if it's solid or not), if the
     * adjacent block is at the given coordinates. Args: blockAccess, x, y, z, side
     */
    public boolean b(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        int i1 = iblockaccess.getTypeId(i, j, k);

        if (!localFlag && i1 == id)
        {
            return false;
        }
        else
        {
            return super.b(iblockaccess, i, j, k, l);
        }
    }
}

