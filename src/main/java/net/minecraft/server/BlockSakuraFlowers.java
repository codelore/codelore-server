/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.minecraft.server;

/**
 *
 * @author Smile
 */
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import org.bukkit.craftbukkit.CraftChunk;

public class BlockSakuraFlowers extends Block {

    
	public BlockSakuraFlowers(int i) {
		super(i, Material.LEAVES);
                this.a(CreativeModeTab.b);
	}
	
	public boolean renderAsNormalBlock() {
		return false;
	}

	public boolean isOpaqueCube() {
		return false;
	}
	  public void randomDisplayTick(World world, int i, int j, int k, Random random)
    {
                Calendar cal = Calendar.getInstance();
                Date now = new Date();
                cal.setTime(now);
                int dayofweek = cal.get(Calendar.DAY_OF_WEEK);
                if (dayofweek == cal.FRIDAY)
                {
                if(random.nextInt(32) != 0){
    		return;
                }
                int l = world.getTypeId(i, j, k);
                if((l & 0x4) != 0) {
                    double d = (float)i + random.nextFloat();
                    double d1 = (float)j + 0.0F;
                    double d2 = (float)k + random.nextFloat();
                    world.addParticle("sakura", d, d1, d2, 0.0D, 0.0D, 0.0D); 
                }
                }
                else
                {
                world.addParticle(null, minX, minX, minX, minX, minX, minX);    
                }
    }
}