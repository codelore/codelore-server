/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.minecraft.server;

import java.util.Random;

/**
 *
 * @author Smile
 */
public class BlockTulpan extends Block
{
    protected BlockTulpan(int i)
    {
        super(i, Material.PLANT);
        this.b(true);
        float f = 0.2F;
        a(0.5F - f, 0.0F, 0.5F - f, 0.5F + f, f * 3F, 0.5F + f);
    }

    /**
     * Checks to see if its valid to put this block at the specified coordinates. Args: world, x, y, z
     */
    public boolean canPlace(World world, int i, int j, int k)
    {
        return super.canPlace(world, i, j, k) && canThisPlantGrowOnThisBlockID(world.getTypeId(i, j - 1, k));
    }

    protected boolean canThisPlantGrowOnThisBlockID(int i)
    {
        return i == Block.GRASS.id || i == Block.DIRT.id || i == Block.SOIL.id;
    }

    /**
     * Lets the block know when one of its neighbor changes. Doesn't know which neighbor changed (coordinates passed are
     * their own) Args: x, y, z, neighbor blockID
     */
    public void doPhysics(World world, int i, int j, int k, int l)
    {
        super.doPhysics(world, i, j, k, l);
        checkFlowerChange(world, i, j, k);
    }

    /**
     * Ticks the block if it's been scheduled
     */
    public void a(World world, int i, int j, int k, Random random)
    {
        byte byte0 = 4;
        int l = 5;

        for (int i1 = i - byte0; i1 <= i + byte0; i1++)
        {
            for (int k1 = k - byte0; k1 <= k + byte0; k1++)
            {
                for (int i2 = j - 1; i2 <= j + 1; i2++)
                {
                    if (world.getTypeId(i1, i2, k1) == id && --l <= 0)
                    {
                        return;
                    }
                }
            }
        }

        int j1 = (i + random.nextInt(3)) - 1;
        int l1 = (j + random.nextInt(2)) - random.nextInt(2);
        int j2 = (k + random.nextInt(3)) - 1;

        for (int k2 = 0; k2 < 4; k2++)
        {
            if (world.isEmpty(j1, l1, j2) && this.d(world, j1, l1, j2))
            {
                i = j1;
                j = l1;
                k = j2;
            }

            j1 = (i + random.nextInt(3)) - 1;
            l1 = (j + random.nextInt(2)) - random.nextInt(2);
            j2 = (k + random.nextInt(3)) - 1;
        }

        if (world.isEmpty(j1, l1, j2) && this.d(world, j1, l1, j2) && random.nextInt(5) == 0)
        {
            world.setTypeId(j1, l1, j2, Block.blockTulpan.id);
        }

        checkFlowerChange(world, i, j, k);
    }

    protected final void checkFlowerChange(World world, int i, int j, int k)
    {
        if (!this.d(world, i, j, k))
        {
            b(world, i, j, k, world.getData(i, j, k), 0);
            world.setTypeId(i, j, k, 0);
        }
    }

    /**
     * Can this block stay at this position.  Similar to canPlaceBlockAt except gets checked often with plants.
     */
 public boolean d(World world, int i, int j, int k) {
       return (world.k(i, j, k) >= 8 || world.isChunkLoaded(i, j)) && canThisPlantGrowOnThisBlockID(world.getTypeId(i, j - 1, k));
    }
    /**
     * Returns a bounding box from the pool of bounding boxes (this means this box can change after the pool has been
     * cleared to be reused)
     */
    public AxisAlignedBB e(World world, int i, int j, int k)
    {
        return null;
    }

    /**
     * Returns the ID of the items to drop on destruction.
     */
    public int getDropType(int i, Random random, int j)
    {
        return Block.blockTulpan.id;
    }

    /**
     * Is this block (a) opaque and (b) a full 1m cube?  This determines whether or not to render the shared face of two
     * adjacent blocks and also whether the player can attach torches, redstone wire, etc to this block.
     */

    /**
     * If this block doesn't render as an ordinary block it will return False (examples: signs, buttons, stairs, etc)
     */
    public boolean b()
    {
        return false;
    }

    /**
     * The type of render function that is called for this block
     */
}
