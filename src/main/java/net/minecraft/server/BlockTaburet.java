/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.minecraft.server;

/**
 *
 * @author Smile
 */
import java.util.Random;

public class BlockTaburet extends Block
{
    private boolean localFlag;

    public BlockTaburet(int i)
    {
        super(i, Material.WOOD);
        this.a(CreativeModeTab.c);
        a(0.0F, 0.0F, 0.0F, 1.0F, 0.5F, 1.0F);
    }

    public boolean canBePushed()
    {
        return true;
    }

    public int idDropped(int i, Random random)
    {
        return Block.blockTaburet.id;
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int a(Random random)
    {
        return 1;
    }

    /**
     * From the specified side and block metadata retrieves the blocks texture. Args: side, metadata
     */
    public int a(int i, int j)
    {
        if (i == 1)
        {
            return textureId;
        }

        if (i == 0)
        {
            return textureId;
        }

        int k = Block.blockTaburetSide;

        if (j == 2 && i == 2)
        {
            return k;
        }

        if (j == 3 && i == 5)
        {
            return k;
        }

        if (j == 0 && i == 3)
        {
            return k;
        }

        if (j == 1 && i == 4)
        {
            return k;
        }
        else
        {
            return Block.blockTaburetSide;
        }
    }

    /**
     * Returns the block texture based on the side being looked at.  Args: side
     */
    public int a(int i)
    {
        if (i == 1)
        {
            return textureId;
        }

        if (i == 0)
        {
            return textureId;
        }

        if (i == 3)
        {
            return Block.blockTaburetSide;
        }
        else
        {
            return Block.blockTaburetSide;
        }
    }

    /**
     * Is this block (a) opaque and (b) a full 1m cube?  This determines whether or not to render the shared face of two
     * adjacent blocks and also whether the player can attach torches, redstone wire, etc to this block.
     */
    public boolean isOpaqueCube()
    {
        return false;
    }

    /**
     * Returns Returns true if the given side of this block type should be rendered (if it's solid or not), if the
     * adjacent block is at the given coordinates. Args: blockAccess, x, y, z, side
     */
    public boolean b(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        int i1 = iblockaccess.getTypeId(i, j, k);

        if (!localFlag && i1 == id)
        {
            return false;
        }
        else
        {
            return super.b(iblockaccess, i, j, k, l);
        }
    }
}
