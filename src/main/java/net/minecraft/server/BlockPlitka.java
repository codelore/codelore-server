/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.minecraft.server;

/**
 *
 * @author Smile
 */
import java.util.Random;


public class BlockPlitka extends Block
{

    private boolean localFlag;

    public BlockPlitka(int i)
    {
        super(i, Material.STONE);
        this.a(CreativeModeTab.b);
        a(0.0F, 0.0F, 0.0F, 1.0F, 0.1F, 1.0F);
    }

    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public int idDropped(int i, Random random)
    {
        return Block.blockPlitka.id;
    }

    public int quantityDropped(Random random)
    {
        return 1;
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

     public boolean b(IBlockAccess iblockaccess, int i, int j, int k, int l)
    {
        int i1 = iblockaccess.getTypeId(i, j, k);

        if (!localFlag && i1 == id)
        {
            return false;
        }
        else
        {
            return super.b(iblockaccess, i, j, k, l);
        }
    }
}

