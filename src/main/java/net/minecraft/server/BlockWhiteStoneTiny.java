/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.minecraft.server;

/**
 *
 * @author Smile
 */
import java.util.Random;

public class BlockWhiteStoneTiny extends Block
{
    public BlockWhiteStoneTiny(int i)
    {
        super(i, Material.STONE);
        this.a(CreativeModeTab.b);
    }
    /**
     * Returns the ID of the items to drop on destruction.
     */
    public int getDropType(int i, Random random, int j)
    {
    	return ((random.nextInt(4) == 1) ? Block.itemWhiteStone.id : Block.blockWhiteStoneTiny.id); 
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    
}
