/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.minecraft.server;

/**
 *
 * @author Smile
 */
import java.util.Random;

public class BlockWhiteStone extends Block
{
    public BlockWhiteStone(int i)
    {
        super(i, Material.STONE);
        this.a(CreativeModeTab.b);
        this.b(true);
    }
    /**
     * Returns the ID of the items to drop on destruction.
     */
    public int getDropType(int i, Random random, int j)
    {
    	return Block.itemWhiteStone.id;
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int a(Random random)
    {
        return 1 + random.nextInt(4);
    }
    public void a(World world, int i, int j, int k, Random random)
    {
    	if(world.getLightLevel(i, j + 1, k) >= 12 && world.getTypeId(i, j + 1, k) == 0 && world.getTypeId(i, j + 2, k) == 0 && random.nextInt(100) == 53 )
        {
            world.setTypeId(i, j, k, Block.blockWhiteStoneTiny.id);
        } 
    }
}
