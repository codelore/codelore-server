/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.minecraft.server;

/**
 *
 * @author Smile
 */
import java.util.Random;


public class BlockPlita extends Block
{

    public BlockPlita(int i)
    {
        super(i, Material.STONE);
        this.a(CreativeModeTab.b);
    }

    public int idDropped(int i, Random random, int j)
    {
        return Block.itemPlitka.id;
    }

    public int quantityDropped(Random random)
    {
        return 1 + random.nextInt(3);
    }
}
