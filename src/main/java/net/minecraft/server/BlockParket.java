/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.minecraft.server;

/**
 *
 * @author Smile
 */
import java.util.Random;

public class BlockParket extends Block
{
    public BlockParket(int i)
    {
        super(i, Material.WOOD);
        this.a(CreativeModeTab.b);
    }

    public int idDropped(int i, int j)
    {
        return Block.blockParket.id;
    }

    /**
     * Returns the quantity of items to drop on block destruction.
     */
    public int a(Random random)
    {
        return 1;
    }
}